/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.interface1;

/**
 *
 * @author AdMiN
 */
public class Testlist {
    public static void main(String[] args) {
        Bat bat = new Bat("BATMAN");
        Cat cat = new Cat("Plam");
        Bird bird = new Bird("Mos");
        Crab crab = new Crab("Ni");
        Crocodile crocodile = new Crocodile("Jed");
        Dog dog = new Dog("Nick");
        Fish fish = new Fish("Art");
        Human human = new Human("Hong");
        Snake snake = new Snake("Green");
        Car car = new Car("Phoshe");
        Plane plane = new Plane("RED");
        Flyable[] flyable = {bat, bird, plane};
       for (Flyable f : flyable) {
            if (f instanceof Poultry) {
                Poultry p = (Poultry) f;
                p.eat();
                p.walk();
                p.fly();
                p.speak();
                p.sleep();
            } else if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
                p.stopEngine();
            }

            }
        }
}

