/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.interface1;

/**
 *
 * @author AdMiN
 */
public class Car extends Verchicle implements Runable {

    public Car() {
        super("Car");
    }
    @Override
    public void startEngine() {
        System.out.println("Car: Start Engine.");
    }
    @Override
    public void stopEngine() {
        System.out.println("Car: Stop Engine.");
    }
    @Override
    public void run() {
        System.out.println("Car: Run.");
    }
}
